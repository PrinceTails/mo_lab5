﻿using System;

namespace MO_lab5
{
    class Program
    {
        const double eps = 1e-4;
        static int M = 500;

        static void Main(string[] args)
        {
            penaltyFuct(-0.5, 1.5);
            factors(-0.5, 1.5);
        }

        #region calculate functions
        static double fx(double x1, double x2)
        {
            return 10 * x1 - x2;
        }
        static double g1(double x1, double x2)
        {
            return x1 * x1 - 4 * x1 + x2 * x2 + 3;
        }
        static double g2(double x1, double x2)
        {
            return x1 * x1 + 4 * x2 * x2 - 4 * x2 - 4;
        }
        #endregion

        #region Gradient descent
        static int Ndx = 0;
        static int Nx = 0;
        static double Dihotomia(double a0, double b0, double[] x,bool factors)
        {
            int k;
            double lk, mk;
            double delta = 0.5 * eps;
            double x_;
            double ak = a0, bk = b0;
            k = 1;
            do
            {
                lk = (ak + bk - delta) / 2;
                mk = (ak + bk + delta) / 2;

                k++;
                if (g(x[0], x[1], lk,factors) <= g(x[0], x[1], mk,factors))
                {
                    bk = mk;
                }
                else
                {
                    ak = lk;
                }
                Nx++;
                Ndx += 4;
            } while ((bk - ak) >= eps);

            x_ = (ak + bk) / 2;

            return x_;
        }
        static double g(double x1, double x2, double alpha, bool factors)
        {
            if (factors)
                return Q(x1 - alpha * Qdx1(x1, x2), x2 - alpha * Qdx2(x1, x2));
            else
                return Fx(x1 - alpha * Fdx1(x1, x2), x2 - alpha * Fdx2(x1, x2));
        }
        static double Norm(double[] x)
        {
            return Math.Sqrt(x[0] * x[0] + x[1] * x[1]);
        }
        static double [] GreatDescent(double[] x, bool factors)
        {
            double alpha = 0;
            int k;
            
            double[] Xk = x;
            double[] Xk1 = new double[2];

            for (k = 0; k < M - 2; k++)
            {
                alpha = Dihotomia(-100, 100, Xk, factors);
                if (factors)
                {
                    Xk1[0] = Xk[0] - alpha * Qdx1(Xk[0], Xk[1]);
                    Xk1[1] = Xk[1] - alpha * Qdx2(Xk[0], Xk[1]);
                }
                else
                {
                    Xk1[0] = Xk[0] - alpha * Fdx1(Xk[0], Xk[1]);
                    Xk1[1] = Xk[1] - alpha * Fdx2(Xk[0], Xk[1]);
                }
                if (k > 1)
                {
                    if (Norm(new double[] { Xk1[0] - Xk[0], Xk1[1] - Xk[1] }) < eps)
                    {
                        break;
                    }
                    Xk = Xk1;
                }
            }
            return Xk1;
        }
        #endregion

        #region Penalty fuctions
        static double r1, r2;
        static void penaltyFuct(double x1, double x2)
        {
            r1 = 1.0;
            r2 = 1.0;
            double h = 0.1;
            double[] Xk = new double[] { x1, x2 };
            double[] Xk1 = new double[Xk.GetLength(0)];
            int k = 0;
            do
            {
                if (k > M)
                    break;
                r1 += h;
                r2 += h;
                Xk1 = GreatDescent(Xk, false);
                Xk = Xk1;
                k++;
            } while (!Cg1(Xk1[0], Xk1[1]) || !Cg2(Xk1[0], Xk1[1]));
            Console.WriteLine("1. Penalty functions method");
            Console.WriteLine("X*    = ({0};{1}) \nf(X*) = {2}", Xk1[0], Xk1[1], Fx(Xk1[0], Xk1[1]));
            Console.WriteLine("Count of iterations: {0} \nCount calculate fx: {1} \nCount calculate fdx: {2}", k, Nx, Ndx);
        }
        #region calculate for PF
        static double Fx(double x1, double x2)
        {
            return fx(x1, x2) + r1 * G1(x1, x2) + r2 * G2(x1, x2);
        }
        static double G1(double x1, double x2)
        {
            double t = g1(x1, x2);
            return (t + Math.Abs(t)) / 2;
        }
        static double G2(double x1, double x2)
        {
            double t = g2(x1, x2);
            return (t + Math.Abs(t)) / 2;
        }
        static bool Cg1(double x1, double x2)
        {
            return g1(x1, x2) <= 0;
        }
        static bool Cg2(double x1, double x2)
        {
            return g2(x1, x2) <= 0;
        }
        private static double Fdx2(double x1, double x2)
        {
            double t1 = g1(x1, x2);
            double t2 = g2(x1, x2);

            return -1 + t1 + Math.Abs(t1) + (r1 / 2) * (2 * x2 + (2 * x2 * t1) / Math.Abs(t1)) + t2 + Math.Abs(t2) + (r2 / 2) * (8 * x2 - 4 + ((8 * x2 - 4) * t2) / Math.Abs(t2));

        }
        private static double Fdx1(double x1, double x2)
        {
            double t1 = g1(x1, x2);
            double t2 = g2(x1, x2);
            return 10 + t1 + Math.Abs(t1) + (r1 / 2) * (2 * x1 - 4 + (2 * (x1 - 2) * t1) / Math.Abs(t1)) + t2 + Math.Abs(t2) + (r2 / 2) * (2 * x1 + (2 * x1 * t2) / Math.Abs(t2));
        }
        #endregion
        #endregion

        #region Factors
        static double s1, s2, R;
        static void factors(double x1,double x2)
        {
            s1 = 0;
            s2 = 0;
            R = 2.5;
            Nx = 0;
            Ndx = 0;
            double[] Xk = new double[] { x1, x2 };
            double[] Xk1 = new double[Xk.GetLength(0)];
            int k = 1;
            Xk1 = GreatDescent(Xk,true);
            s1 = sigma1(Xk1[0], Xk1[1]);
            s2 = sigma2(Xk1[0], Xk1[1]);
            while (!isEnd(Xk,Xk1))
            {
                Xk = Xk1;
                if (k > M)
                    break;
                Xk1 = GreatDescent(Xk,true);
                s1 = sigma1(Xk1[0], Xk1[1]);
                s2 = sigma2(Xk1[0], Xk1[1]);
                k++;
            }
            Console.WriteLine("2. Factors method");
            Console.WriteLine("X*    = ({0};{1}) \nf(X*) = {2}", Xk1[0], Xk1[1], Q(Xk1[0], Xk1[1]));
            Console.WriteLine("Count of iterations: {0} \nCount calculate fx: {1} \nCount calculate fdx: {2}", k, Nx, Ndx);
        }
        static bool isEnd(double [] Xk, double [] Xk1)
        {
            if (fx(Xk1[0], Xk1[1]) - fx(Xk[0], Xk[1]) <= eps)
                return true;
            else if (g1(Xk[0], Xk[1]) - g1(Xk1[0], Xk1[1]) <= eps)
                return true;
            else if (g2(Xk[0], Xk[1]) - g2(Xk1[0], Xk1[1]) <= eps)
                return true;
            else
                return false;
        }
        #region calculate for F
        static double sigma1(double x1, double x2)
        {
            double t = g1(x1, x2) + s1;
            if (t >= 0)
                return t;
            else
                return 0;
        }
        static double sigma2(double x1, double x2)
        {
            double t = g2(x1, x2) + s2;
            if (t >= 0)
                return t;
            else
                return 0;
        }
        static double Q(double x1, double x2)
        {
            return 10 * x1 - x2 + R * (Math.Pow(sigma1(x1, x2), 2) - s1 + Math.Pow(sigma2(x1, x2), 2) - s2);
        }
        static double Qdx1(double x1, double x2)
        {
            return 10 + R * ((4 * x1 - 8) * sigma1(x1, x2) + 4 * x1 * sigma2(x1, x2));
        }
        static double Qdx2(double x1, double x2)
        {
            return -1 + R * (4*x2 * sigma1(x1, x2) + (16*x2-8) * sigma2(x1, x2));
        }
        #endregion
        #endregion
    }
}
